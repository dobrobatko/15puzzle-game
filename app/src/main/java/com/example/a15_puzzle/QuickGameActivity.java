package com.example.a15_puzzle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Objects;
import java.util.Random;

public class QuickGameActivity extends AppCompatActivity {

    public int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).hide(); // <-- Hide TitleBar
        setContentView(R.layout.activity_quick_game);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); // <-- Lock Orientation
    }

    public void onClick15(View view) {

        Button button15 = findViewById(R.id.puzzButton15);
        Button button11 = findViewById(R.id.puzzButton11);
        Button button14 = findViewById(R.id.puzzButton14);
        Button button16 = findViewById(R.id.puzzButton16);

        if (button16.getText().equals("0")) {
            button16.setText(button15.getText());
            button15.setText("0");
            button15.setVisibility(View.INVISIBLE);
            button16.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button14.getText().equals("0")) {
            button14.setText(button15.getText());
            button15.setText("0");
            button15.setVisibility(View.INVISIBLE);
            button14.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button11.getText().equals("0")) {
            button11.setText(button15.getText());
            button15.setText("0");
            button15.setVisibility(View.INVISIBLE);
            button11.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick16(View view) {

        Button button16 = findViewById(R.id.puzzButton16);
        Button button12 = findViewById(R.id.puzzButton12);
        Button button15 = findViewById(R.id.puzzButton15);


        if (button15.getText().equals("0")) {
            button15.setText(button16.getText());
            button16.setText("0");
            button16.setVisibility(View.INVISIBLE);
            button15.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button12.getText().equals("0")) {
            button12.setText(button16.getText());
            button16.setText("0");
            button16.setVisibility(View.INVISIBLE);
            button12.setVisibility(View.VISIBLE);
            checkCounter();
        }
    }

    public void onClick14(View view) {

        Button button14 = findViewById(R.id.puzzButton14);
        Button button13 = findViewById(R.id.puzzButton13);
        Button button10 = findViewById(R.id.puzzButton10);
        Button button15 = findViewById(R.id.puzzButton15);

        if (button13.getText().equals("0")) {
            button13.setText(button14.getText());
            button14.setText("0");
            button14.setVisibility(View.INVISIBLE);
            button13.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button10.getText().equals("0")) {
            button10.setText(button14.getText());
            button14.setText("0");
            button14.setVisibility(View.INVISIBLE);
            button10.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button15.getText().equals("0")) {
            button15.setText(button14.getText());
            button14.setText("0");
            button14.setVisibility(View.INVISIBLE);
            button15.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick13(View view) {

        Button button13 = findViewById(R.id.puzzButton13);
        Button button9 = findViewById(R.id.puzzButton9);
        Button button14 = findViewById(R.id.puzzButton14);


        if (button9.getText().equals("0")) {
            button9.setText(button13.getText());
            button13.setText("0");
            button13.setVisibility(View.INVISIBLE);
            button9.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button14.getText().equals("0")) {
            button14.setText(button13.getText());
            button13.setText("0");
            button13.setVisibility(View.INVISIBLE);
            button14.setVisibility(View.VISIBLE);
            checkCounter();
        }
    }

    public void onClick12(View view) {

        Button button12 = findViewById(R.id.puzzButton12);
        Button button8 = findViewById(R.id.puzzButton8);
        Button button11 = findViewById(R.id.puzzButton11);
        Button button16 = findViewById(R.id.puzzButton16);

        if (button8.getText().equals("0")) {
            button8.setText(button12.getText());
            button12.setText("0");
            button12.setVisibility(View.INVISIBLE);
            button8.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button11.getText().equals("0")) {
            button11.setText(button12.getText());
            button12.setText("0");
            button12.setVisibility(View.INVISIBLE);
            button11.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button16.getText().equals("0")) {
            button16.setText(button12.getText());
            button12.setText("0");
            button12.setVisibility(View.INVISIBLE);
            button16.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick11(View view) {

        Button button11 = findViewById(R.id.puzzButton11);
        Button button10 = findViewById(R.id.puzzButton10);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button12 = findViewById(R.id.puzzButton12);
        Button button15 = findViewById(R.id.puzzButton15);

        if (button10.getText().equals("0")) {
            button10.setText(button11.getText());
            button11.setText("0");
            button11.setVisibility(View.INVISIBLE);
            button10.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button7.getText().equals("0")) {
            button7.setText(button11.getText());
            button11.setText("0");
            button11.setVisibility(View.INVISIBLE);
            button7.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button12.getText().equals("0")) {
            button12.setText(button11.getText());
            button11.setText("0");
            button11.setVisibility(View.INVISIBLE);
            button12.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button15.getText().equals("0")) {
            button15.setText(button11.getText());
            button11.setText("0");
            button11.setVisibility(View.INVISIBLE);
            button15.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick10(View view) {

        Button button10 = findViewById(R.id.puzzButton10);
        Button button9 = findViewById(R.id.puzzButton9);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button11 = findViewById(R.id.puzzButton11);
        Button button14 = findViewById(R.id.puzzButton14);

        if (button9.getText().equals("0")) {
            button9.setText(button10.getText());
            button10.setText("0");
            button10.setVisibility(View.INVISIBLE);
            button9.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button6.getText().equals("0")) {
            button6.setText(button10.getText());
            button10.setText("0");
            button10.setVisibility(View.INVISIBLE);
            button6.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button11.getText().equals("0")) {
            button11.setText(button10.getText());
            button10.setText("0");
            button10.setVisibility(View.INVISIBLE);
            button11.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button14.getText().equals("0")) {
            button14.setText(button10.getText());
            button10.setText("0");
            button10.setVisibility(View.INVISIBLE);
            button14.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick9(View view) {

        Button button9 = findViewById(R.id.puzzButton9);
        Button button5 = findViewById(R.id.puzzButton5);
        Button button10 = findViewById(R.id.puzzButton10);
        Button button13 = findViewById(R.id.puzzButton13);

        if (button5.getText().equals("0")) {
            button5.setText(button9.getText());
            button9.setText("0");
            button9.setVisibility(View.INVISIBLE);
            button5.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button10.getText().equals("0")) {
            button10.setText(button9.getText());
            button9.setText("0");
            button9.setVisibility(View.INVISIBLE);
            button10.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button13.getText().equals("0")) {
            button13.setText(button9.getText());
            button9.setText("0");
            button9.setVisibility(View.INVISIBLE);
            button13.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick8(View view) {

        Button button8 = findViewById(R.id.puzzButton8);
        Button button4 = findViewById(R.id.puzzButton4);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button12 = findViewById(R.id.puzzButton12);

        if (button4.getText().equals("0")) {
            button4.setText(button8.getText());
            button8.setText("0");
            button8.setVisibility(View.INVISIBLE);
            button4.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button7.getText().equals("0")) {
            button7.setText(button8.getText());
            button8.setText("0");
            button8.setVisibility(View.INVISIBLE);
            button7.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button12.getText().equals("0")) {
            button12.setText(button8.getText());
            button8.setText("0");
            button8.setVisibility(View.INVISIBLE);
            button12.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick7(View view) {

        Button button7 = findViewById(R.id.puzzButton7);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button3 = findViewById(R.id.puzzButton3);
        Button button8 = findViewById(R.id.puzzButton8);
        Button button11 = findViewById(R.id.puzzButton11);

        if (button6.getText().equals("0")) {
            button6.setText(button7.getText());
            button7.setText("0");
            button7.setVisibility(View.INVISIBLE);
            button6.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button3.getText().equals("0")) {
            button3.setText(button7.getText());
            button7.setText("0");
            button7.setVisibility(View.INVISIBLE);
            button3.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button8.getText().equals("0")) {
            button8.setText(button7.getText());
            button7.setText("0");
            button7.setVisibility(View.INVISIBLE);
            button8.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button11.getText().equals("0")) {
            button11.setText(button7.getText());
            button7.setText("0");
            button7.setVisibility(View.INVISIBLE);
            button11.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick6(View view) {

        Button button6 = findViewById(R.id.puzzButton6);
        Button button5 = findViewById(R.id.puzzButton5);
        Button button2 = findViewById(R.id.puzzButton2);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button10 = findViewById(R.id.puzzButton10);

        if (button5.getText().equals("0")) {
            button5.setText(button6.getText());
            button6.setText("0");
            button6.setVisibility(View.INVISIBLE);
            button5.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button2.getText().equals("0")) {
            button2.setText(button6.getText());
            button6.setText("0");
            button6.setVisibility(View.INVISIBLE);
            button2.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button7.getText().equals("0")) {
            button7.setText(button6.getText());
            button6.setText("0");
            button6.setVisibility(View.INVISIBLE);
            button7.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button10.getText().equals("0")) {
            button10.setText(button6.getText());
            button6.setText("0");
            button6.setVisibility(View.INVISIBLE);
            button10.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick5(View view) {

        Button button5 = findViewById(R.id.puzzButton5);
        Button button1 = findViewById(R.id.puzzButton1);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button9 = findViewById(R.id.puzzButton9);

        if (button1.getText().equals("0")) {
            button1.setText(button5.getText());
            button5.setText("0");
            button5.setVisibility(View.INVISIBLE);
            button1.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button6.getText().equals("0")) {
            button6.setText(button5.getText());
            button5.setText("0");
            button5.setVisibility(View.INVISIBLE);
            button6.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button9.getText().equals("0")) {
            button9.setText(button5.getText());
            button5.setText("0");
            button5.setVisibility(View.INVISIBLE);
            button9.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick4(View view) {

        Button button4 = findViewById(R.id.puzzButton4);
        Button button3 = findViewById(R.id.puzzButton3);
        Button button8 = findViewById(R.id.puzzButton8);


        if (button3.getText().equals("0")) {
            button3.setText(button4.getText());
            button4.setText("0");
            button4.setVisibility(View.INVISIBLE);
            button3.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button8.getText().equals("0")) {
            button8.setText(button4.getText());
            button4.setText("0");
            button4.setVisibility(View.INVISIBLE);
            button8.setVisibility(View.VISIBLE);
            checkCounter();
        }
    }

    public void onClick3(View view) {

        Button button3 = findViewById(R.id.puzzButton3);
        Button button2 = findViewById(R.id.puzzButton2);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button4 = findViewById(R.id.puzzButton4);

        if (button2.getText().equals("0")) {
            button2.setText(button3.getText());
            button3.setText("0");
            button3.setVisibility(View.INVISIBLE);
            button2.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button7.getText().equals("0")) {
            button7.setText(button3.getText());
            button3.setText("0");
            button3.setVisibility(View.INVISIBLE);
            button7.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button4.getText().equals("0")) {
            button4.setText(button3.getText());
            button3.setText("0");
            button3.setVisibility(View.INVISIBLE);
            button4.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick2(View view) {

        Button button2 = findViewById(R.id.puzzButton2);
        Button button1 = findViewById(R.id.puzzButton1);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button3 = findViewById(R.id.puzzButton3);

        if (button1.getText().equals("0")) {
            button1.setText(button2.getText());
            button2.setText("0");
            button2.setVisibility(View.INVISIBLE);
            button1.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button6.getText().equals("0")) {
            button6.setText(button2.getText());
            button2.setText("0");
            button2.setVisibility(View.INVISIBLE);
            button6.setVisibility(View.VISIBLE);
            checkCounter();
        }
        if (button3.getText().equals("0")) {
            button3.setText(button2.getText());
            button2.setText("0");
            button2.setVisibility(View.INVISIBLE);
            button3.setVisibility(View.VISIBLE);
            checkCounter();
        }

    }

    public void onClick1(View view) {

        Button button1 = findViewById(R.id.puzzButton1);
        Button button2 = findViewById(R.id.puzzButton2);
        Button button5 = findViewById(R.id.puzzButton5);


        if (button2.getText().equals("0")) {
            button2.setText(button1.getText());
            button1.setText("0");
            button1.setVisibility(View.INVISIBLE);
            button2.setVisibility(View.VISIBLE);
            checkCounter();

        }
        if (button5.getText().equals("0")) {
            button5.setText(button1.getText());
            button1.setText("0");
            button1.setVisibility(View.INVISIBLE);
            button5.setVisibility(View.VISIBLE);
            checkCounter();
        }
    }

    public void mixClick(View view) {

        Button button1 = findViewById(R.id.puzzButton1);
        Button button2 = findViewById(R.id.puzzButton2);
        Button button3 = findViewById(R.id.puzzButton3);
        Button button4 = findViewById(R.id.puzzButton4);
        Button button5 = findViewById(R.id.puzzButton5);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button8 = findViewById(R.id.puzzButton8);
        Button button9 = findViewById(R.id.puzzButton9);
        Button button10 = findViewById(R.id.puzzButton10);
        Button button11 = findViewById(R.id.puzzButton11);
        Button button12 = findViewById(R.id.puzzButton12);
        Button button13 = findViewById(R.id.puzzButton13);
        Button button14 = findViewById(R.id.puzzButton14);
        Button button15 = findViewById(R.id.puzzButton15);
        Button button16 = findViewById(R.id.puzzButton16);

        int i = 1000;
        while (i > 0) {
            Random random = new Random();
            int jumps = random.nextInt(16)+1;
            switch (jumps) {
                case 1: button1.performClick();
                    break;
                case 2: button2.performClick();
                    break;
                case 3: button3.performClick();
                    break;
                case 4: button4.performClick();
                    break;
                case 5: button5.performClick();
                    break;
                case 6: button6.performClick();
                    break;
                case 7: button7.performClick();
                    break;
                case 8: button8.performClick();
                    break;
                case 9: button9.performClick();
                    break;
                case 10: button10.performClick();
                    break;
                case 11: button11.performClick();
                    break;
                case 12: button12.performClick();
                    break;
                case 13: button13.performClick();
                    break;
                case 14: button14.performClick();
                    break;
                case 15: button15.performClick();
                    break;
                case 16: button16.performClick();
                    break;
            }
            i--;
            counter = 0;
        }

    }

    public void quitQuickGame(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void checkCounter() {

        Button button1 = findViewById(R.id.puzzButton1);
        Button button2 = findViewById(R.id.puzzButton2);
        Button button3 = findViewById(R.id.puzzButton3);
        Button button4 = findViewById(R.id.puzzButton4);
        Button button5 = findViewById(R.id.puzzButton5);
        Button button6 = findViewById(R.id.puzzButton6);
        Button button7 = findViewById(R.id.puzzButton7);
        Button button8 = findViewById(R.id.puzzButton8);
        Button button9 = findViewById(R.id.puzzButton9);
        Button button10 = findViewById(R.id.puzzButton10);
        Button button11 = findViewById(R.id.puzzButton11);
        Button button12 = findViewById(R.id.puzzButton12);
        Button button13 = findViewById(R.id.puzzButton13);
        Button button14 = findViewById(R.id.puzzButton14);
        Button button15 = findViewById(R.id.puzzButton15);
        String message1 = getString((R.string.doneMessage1));
        String message2 = getString(R.string.doneMessage2);

        counter++;

        if(button1.getText().equals("1") &&
                button2.getText().equals("2") &&
                button3.getText().equals("3") &&
                button4.getText().equals("4") &&
                button5.getText().equals("5") &&
                button6.getText().equals("6") &&
                button7.getText().equals("7") &&
                button8.getText().equals("8") &&
                button9.getText().equals("9") &&
                button10.getText().equals("10") &&
                button11.getText().equals("11") &&
                button12.getText().equals("12") &&
                button13.getText().equals("13") &&
                button14.getText().equals("14") &&
                button15.getText().equals("15"))
            Toast.makeText(this, message1 + " " + counter + " " + message2, Toast.LENGTH_LONG).show();

    }
}